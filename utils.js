const moment = require('moment-timezone');
const constant = require('./constant');

function initOs() {
    if (process.platform === 'darwin' || process.platform === 'win32')
        process.env.DEBUG = 'true';
    else
        process.env.DEBUG = 'false'
}

function isAfter3Hours(date) {
    let now = moment(moment().tz(constant.istanbul).format());

    return now.diff(date, 'minutes') > 15;
}

function fa2en(txt) {
    return txt.replace(/۱/g, "1").replace(/۲/g, "2").replace(/۳/g, "3").replace(/۴/g, "4").replace(/۵/g, "5").replace(/۶/g, "6").replace(/۷/g, "7").replace(/۸/g, "8").replace(/۹/g, "9")
        .replace(/۰/g, "0");

}

 function numberValidation(msg) {

    const st = fa2en(msg.text);
    const number = st;
    let phoneRGEX = RegExp('^[1-9]\\d*$');
    let phoneResult = phoneRGEX.test(number);
    return phoneResult !== false;
}





module.exports = {

    initOs: initOs,
    isAfter3Hours: isAfter3Hours,
    fa2en: fa2en,
    numberValidation:numberValidation


};


