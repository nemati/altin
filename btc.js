let Binance = require('binance-api-node').default
var persianMoment = require('moment-jalaali');
const moment = require('moment-timezone');
var numeral = require('numeral');
const message = require('./message');
const TelegramBot = require('node-telegram-bot-api');
let token = '1281134682:AAF2Q3h7mlQIGUpxC5_X604EKAC-WNzj55Y'
const cron = require('node-cron');
const cronPost = require('node-cron');
const fs = require('fs');
let channel = -1001370526621
const bot = new TelegramBot(token, {polling: true});
let buy = 0
let sell = 0
let lastMessage = 0
let type = [
    {key: 'BTCUSDT', name: 'بیت‌کوین (BTC)‏', fixed: 2, prev: 0},
    {key: 'XRPUSDT', name: 'ریپل (XRP)‏', fixed: 3, prev: 0},
    {key: 'ETHUSDT', name: 'اتریوم (ETH)‏', fixed: 2, prev: 0},
    {key: 'ADAUSDT', name: 'کاردانو (ADA)‏', fixed: 4, prev: 0},
    {key: 'LTCUSDT', name: 'لایت کوین (LTC)‏', fixed: 2, prev: 0},
    {key: 'XMRUSDT', name: 'مونرو (XMR)‏', fixed: 1, prev: 0},
    {key: 'TRXUSDT', name: 'ترون (TRX)', fixed: 5, prev: 0}
]


bot.on('message', (msg) => {
   // console.log(msg)
    let isAdmin = admin(msg)
    if (!isAdmin)
        return true


    processMessage(msg)


});


cron.schedule('*/8 * * * * *', () => {
    cronTask(true)
});

cronPost.schedule(' */15 * * * *', () => {
    cronTask(false)
});

async function processMessage(msg) {

    if (msg.text.includes("start"))
        message.simpleMessage‌Btc(bot, msg, 'لطفا نرخ خرید و فروش را به تومان وارد کنید.')

    else if (msg.text.includes(':')) {
        let prices = msg.text.split(':')
        sell = parseInt(prices[0])
        buy = parseInt(prices[1])
        await createfile(msg.text)
        message.simpleMessage(bot, msg, 'با موفقیت ثبت شد')
    }

}


// Authenticated client, can make signed calls
const client2 = Binance({
    apiKey: 'h71DQoYtlq5nfHCjdSj0GSRaz9Kk2oqnUp0BJYUuXgv5FD6nlkKpwBXpBDSDPAaM',
    apiSecret: 'emVVIZTujNjr5A2ELNZDBAi3DTTkLJ2TQUMUVuGDHIYv1zehClWr4QyODDYis0HQ'
})


async function cronTask(update) {
    await readOrCreate()
    let prices = await client2.prices()
    let text = textGenerator(prices)

    if (update ===true) {
        message.editMessage(bot, channel, lastMessage, text)
    } else {
        let d =await message.createMessage(bot, channel, text);
      //  console.log(d)
        lastMessage = d.message_id;
    }

}


function textGenerator(price) {
    // console.log(price)
    let t = ''
    let endLine = '\n'
    t += '📌 اعلام نرخ ارزهای برتر مارکت'
    t += endLine
    t += '📆 تاریخ: '
    t += getJalaliDate()
    t += endLine
    t += '⏰ ساعت: '
    t += getTime()
    t += endLine
    t += endLine
    t += 'ــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــ'
    t += endLine
    t += priceGenerator(price)
    t += endLine
    t += '➖➖➖➖➖➖➖➖➖➖➖➖➖➖'
    if (parseInt(buy) !==0 && parseInt(sell) !==0)
    {
        t+=endLine
        t+='📌 اعلام نرخ تتر USDT  ▶️▶️▶️'
        t+=endLine
        t+='ــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــــ'
        t+=endLine
        t+='⤴️ نرخ فروش'
        t+=endLine
        t+='🔹 '
        t+=numberize(parseInt(sell))
        t+=' تومان'
        t+=endLine
        t+=endLine
        t+='⤵️ نرخ خرید'
        t+=endLine
        t+='🔹 '
        t+=numberize(parseInt(buy))
        t+=' تومان'
        t+=endLine

        t+=endLine
        t += '➖➖➖➖➖➖➖➖➖➖➖➖➖➖'

    }
    t += endLine
    t += endLine
    t += 'با ما وارد بازار ارز دیجیتال شوید'
  //  console.log(t)
    return t


}


function priceGenerator(price) {
    let t = ''
    for (let i = 0; i < type.length; i++) {
        let p = eval('price.' + type[i].key)
        let val = parseFloat(p).toFixed(type[i].fixed)
        t += val > type[i].prev ? '📉 ' : '📈 '
        t += '*' + type[i].name + '*'
        t += ' '
        t += '$'
        t += val
        t += '\n'
        if (buy === 0 && sell === 0)
            continue

        t += '👈 نرخ فروش: '
        t += numberize(p * sell)
        t += ' تومان'
        t += '\n'
        t += '👈🏼 خرید از مشتری: '
        t += numberize(p * buy)
        t += ' تومان'
        t += '\n'
        t += '\n'
        type[i].prev = val


    }

    return t
}

function numberize(num) {
    return numeral(num).format('0,0');
}

function getJalaliDate() {
    var today = persianMoment();
    return today.format('jYYYY/jMM/jDD');

}

function getTime() {
    return moment().tz('Asia/Tehran').format('HH:mm')

}

function createfile(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile("./p.txt", data, 'utf8', function (err) {
            if (err) reject(err);
            else resolve(data);
        });
    });
}


async function readOrCreate() {
    try {
        let prices = await fs.readFileSync('./p.txt', 'utf8');
        prices = prices.split(':')
        sell = parseInt(prices[0])
        buy = parseInt(prices[1])
        return prices
    } catch (e) {
        await createfile('0:0')
        sell = 0
        buy = 0
        return '0:0'
    }
}

async function admin(msg) {
    let id = typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id
    id = parseInt(id)
    if (id ===  654445610 || id === 84169752 || id === 754245432 || id === 171764222)
        return true
    return false
}

cronTask(false)

