const request = require('request');

let prevUsd = 0;
let prevUsdSatis = 0;
let prevEuro = 0;
let prevEuroSatis = 0;
let prevEuroToUsd = 0;
let prevEuroToUsdSatis = 0;
let buyT = 0;
let sellT = 0

let buyD = 0;
let sellD = 0

let buyE = 0;
let sellE = 0

let buyA = 0;
let sellA = 0


async function getJson() {
    try {
        return await doRequest("http://haremaltin.com/json/all_prices.json");
    } catch (e) {
        throw 'Error getJson'
    }

}


async function getTextData(json, toman) {

    let time = json.USDTRY.tarih.split(' ')
    let text = '🕒 Time: ' + '*' + time[1] + '*' + '\n\n';
    text += 'Currency💱                    Buy   💰    Sell\n\n'


    if (buyD !== 0 && sellD !== 0) {



        text += '*USD🇺🇸/IRR🇮🇷*  :          ' + buyD + '     ' + sellD + "\n";
        text += '\n\n'
    }


    if (buyE !== 0 && sellE !== 0) {



        text += '*EUR🇪🇺/IRR🇮🇷*  :          ' + buyE + '     ' + sellE + "\n";
        text += '\n\n'
    }

    if (buyT !== 0 && sellT !== 0) {



        text += '*TRY🇹🇷/IRR🇮🇷*  :            ' + buyT + '       ' + sellT + "\n";
        text += '\n\n'
    }

    if (buyA !== 0 && sellA !== 0) {



        text += '*AED🇦🇪/IRR🇮🇷*  :            ' + buyA + '      ' + sellA + "\n";
        text += '\n\n'
    }



    text += '*USD🇺🇸/TRY🇹🇷*  :'

    if (typeof json.USDTRY === "undefined") {
        json.USDTRY={alis:prevUsd,satis:prevUsdSatis}

    }
    text += (json.USDTRY.alis === prevUsd ? ' ➖' : (((json.USDTRY.alis > prevUsd) ? ' ⬆️' : ' 🔻'))) + '  ' + json.USDTRY.alis + '   ' + json.USDTRY.satis + "\n";
    prevUsd = json.USDTRY.alis
    prevUsdSatis = json.USDTRY.satis
    text += '\n\n'
    text += '*EUR🇪🇺/TRY🇹🇷*  :'

    if (typeof json.EURTRY === "undefined") {
        json.EURTRY={alis:prevEuro,satis:prevEuroSatis}

    }
    text += (json.EURTRY.alis === prevEuro ? ' ➖' : (((json.EURTRY.alis > prevEuro) ? ' ⬆️' : ' 🔻'))) + '  ' + json.EURTRY.alis + '   ' + json.EURTRY.satis + "\n";
    prevEuro = json.EURTRY.alis
    prevEuroSatis = json.EURTRY.satis
    text += '\n\n'
    text += '*EUR🇪🇺/USD🇺🇸*  :'


    if (typeof json.EURUSD === "undefined") {
        json.EURUSD={alis:prevEuroToUsd,satis:prevEuroToUsdSatis}

    }
    text += (json.EURUSD.alis === prevEuroToUsd ? ' ➖' : (((json.EURUSD.alis > prevEuroToUsd) ? ' ⬆️' : ' 🔻'))) + '  ' + json.EURUSD.alis + '   ' + json.EURUSD.satis + "\n";
    prevEuroToUsd = json.EURUSD.alis
    prevEuroToUsdSatis = json.EURUSD.satis


    text+='\n'
    text+='➖➖➖➖➖➖➖➖➖➖➖➖➖➖'
    text+='\n'
    text+=
        'با توجه به نوسانات و مبلغ حواله' +
        ' اين قیمت ها حدودي مي باشد و براي معامله حتما چك كنيد يا تماس بگيريد📞 ' +
        'ممنون.🙏'


    return text
}

function doRequest(url) {
    return new Promise(function (resolve, reject) {
        request(url, function (error, res, body) {
            try {
                if (!error && res.statusCode === 200) {
                    resolve(JSON.parse(body));
                } else {
                    reject(error);
                }
            } catch (e) {
                reject('Error Request');
            }

        });
    });
}


async function fetchData(json, toman) {
    return await getTextData(json, toman)
};

async function setTeleToman(toman) {
    toman = toman.split(':')
    if (toman.length !== 2)
        return
    buyT = parseInt(toman[1])
    sellT = parseInt(toman[0])
};

async function setDollarToman(toman) {
    toman = toman.split(':')
    if (toman.length !== 2)
        return
    buyD = parseInt(toman[1])
    sellD = parseInt(toman[0])
};

async function setEuroToman(toman) {
    toman = toman.split(':')
    if (toman.length !== 2)
        return
    buyE = parseInt(toman[1])
    sellE = parseInt(toman[0])
};

async function setAedToman(toman) {
    toman = toman.split(':')
    if (toman.length !== 2)
        return
    buyA = parseInt(toman[1])
    sellA = parseInt(toman[0])
};

module.exports = {
    fetchData: fetchData,
    setTeleToman: setTeleToman,
    setDollarToman:setDollarToman,
    setEuroToman:setEuroToman,
    setAedToman:setAedToman
}
