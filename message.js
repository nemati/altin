async function deleteMessage(bot, chatId, msgId) {
    try {
        return await bot.deleteMessage(chatId, msgId)
    }
    catch (e) {
        throw 'Delete Message Problem'
    }

}

async function simpleMessage(bot, msg, text) {
    const opts = {
        reply_markup: {
            keyboard: [
                ['💷 ایجاد پست جدید در گروه'],
                ['💷 ایجاد پست جدید در کانال'],


            ],
            resize_keyboard: true,
            one_time_keyboard: true

        }
    };


    bot.sendMessage(typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id, text, opts)
}

async function simpleMessage‌Btc(bot, msg, text) {



    bot.sendMessage(typeof msg.chat !== 'undefined' ? msg.chat.id : msg.message.chat.id, text)
}

async function createMessage(bot, channelId, text) {
    try {
        return await bot.sendMessage(channelId, text,{parse_mode: 'Markdown'});
    }
    catch (e) {
      //  console.log(e)
        throw 'Create Message Problem'
    }


}

async function editMessage(bot, channelId, lastMsgId, text) {

    try {
        let opts = {

            chat_id: channelId,
            message_id: lastMsgId,
            parse_mode: 'Markdown'

        };

       return await bot.editMessageText(text, opts)
    }
    catch (e) {
     //   console.log(e)
     //   console.log(e)
        throw 'Edit Message Problem'
    }

}


module.exports = {
    createMessage: createMessage,
    editMessage: editMessage,
    deleteMessage: deleteMessage,
    simpleMessage:simpleMessage,
    simpleMessage‌Btc:simpleMessage‌Btc


};
